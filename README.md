# Documentation

Основной репозиторий документации по проекту Megadebt.

# Оглавление
Навигация до нужных мест документации проекта, собранная в одном месте.

## Docs

* [Техническое задание](https://gitlab.com/megadebt/documentation/-/blob/master/Техническое_задание.docx)
* [Test кейсы](https://gitlab.com/megadebt/documentation/-/blob/master/Test_cases.docx)
* [Use кейсы](https://gitlab.com/megadebt/documentation/-/blob/master/Use_cases.docx)

## Backend

* [Архитектура](https://gitlab.com/megadebt/documentation/-/tree/master/Architecture/backend)
* [Описание REST API](https://gitlab.com/megadebt/backend/-/blob/master/README.md)
* [Скрипт для создания БД и схема БД](https://gitlab.com/megadebt/backend/-/tree/master/db/schema)
* [Инструкция по сборке](https://gitlab.com/megadebt/backend/-/blob/master/README.md#инструкция-по-сборке)

## iOS

* [Архитектура](https://gitlab.com/megadebt/documentation/-/tree/master/Architecture/iOS)
* [Инструкция по сборке](https://gitlab.com/megadebt/ios#build-instruction)
